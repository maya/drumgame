﻿using MonoGame.Extended.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using drumgame.core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace drumgame.screens
{
    internal class GameplayScreen : GameScreen
    {
        Texture2D drumInner;
        Texture2D drumOuter;
        Texture2D beatBarLeft;
        private GameMain gameMain => (GameMain)base.Game;

        public GameplayScreen(GameMain game) : base(game)
        { 

        }

        public override void LoadContent()
        {
            base.LoadContent();
            drumInner = Game.Content.Load<Texture2D>("assets/taiko-drum-inner");
            drumOuter = Game.Content.Load<Texture2D>("assets/taiko-drum-outer");
            beatBarLeft = Game.Content.Load<Texture2D>("assets/taiko-bar-left");

        }

        public override void Update(GameTime gameTime)
        {

            return;
        }

        public override void Draw(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            gameMain._spriteBatch.Begin();
            gameMain._spriteBatch.Draw(beatBarLeft, new Vector2(50, 50), Color.White);

            if (keyboardState.IsKeyDown(Keys.A))
            {
                gameMain._spriteBatch.Draw(drumOuter, new Vector2(51, 50), null, Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.FlipHorizontally, 0f);
            }

            if (keyboardState.IsKeyDown(Keys.S))
            {
                gameMain._spriteBatch.Draw(drumInner, new Vector2(51, 50), Color.White);
            } 
            if (keyboardState.IsKeyDown(Keys.K))
            {
                gameMain._spriteBatch.Draw(drumInner, new Vector2(140, 50), null, Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.FlipHorizontally, 0f);
            }
            if (keyboardState.IsKeyDown(Keys.L))
            {
                gameMain._spriteBatch.Draw(drumOuter, new Vector2(140, 50), Color.White);
            }

            gameMain._spriteBatch.End();

            return;
        }
    }
}
