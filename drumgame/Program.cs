﻿using drumgame.core;

namespace drumgame
{
    public static class Program
    {  
        public static void Main(string[] args)
        {
            using var game = new core.GameMain();
            game.Run();
        }

    }
}

